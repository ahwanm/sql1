<?php
//include database connection file
include_once("connect.php");


if(isset($_GET["task"]) && ($_GET["task"] == "delete"))
{
  $id = (int) $_GET["id"];
  $query = "DELETE FROM users WHERE id=$id";
  $status = mysqli_query($conn, $query) or die(mysqli_error());

  if($status)
  {
      $msg = "Data deleted successfully";
      header("location:index.php?msg=$msg");
  }
  else
  {
      echo "Data could not be deleted";
  }
}

include_once("disconnect.php");
?>
